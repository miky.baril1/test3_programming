//Mikael Baril 1844064
	import javafx.application.*;
	import java.util.*;
	import javafx.event.ActionEvent;
	import javafx.event.EventHandler;
	import javafx.stage.*;
	import javafx.scene.*;
	import javafx.scene.paint.*;
	import javafx.scene.control.*;
	import javafx.scene.layout.*;
		
	public class BetsGame{
		private int money;
		//private int moneyLeft;
		
		Random ran = new Random();
		
		public BetsGame(int money) {
			this.money=250;
			
		}

		public int getMoney() {
			
			return this.money;
			
		}
		public String placeBet(String pChoice,int bet) {
			if (money<0){
				return "Player has no more money.";
			}
			int min = 1;
			int max = 6;
			int ranNum = (int)(Math.random() * (max - min +min)+min);
			String result="";
			if(ranNum == 2|| ranNum==4||ranNum==6) {
				result = "Even";
			}
			else {
				
				result = "Odd";
			}
				
			
			if(money<bet) {
				return "Bet is higher than money left.";
				
			}
			else if( bet == 0) {
				return "Invalid amount of money";
			}
			
			else if (pChoice.equals(result)) {
			this.money= this.money+bet;
			return "The dice rolled on "+result+" You won :"+bet+"$";
		}
		else if(!pChoice.equals(result)) {
			this.money=this.money-bet;
			return "The dice rolled on "+result+" You lost :"+bet+"$";
			
		}
			return pChoice;

	}
	}

		

