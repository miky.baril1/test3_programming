import java.util.Random;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class BetsApp extends Application{
	private TextField message;
	Random ran = new Random();
	
	public void start(Stage stage) throws Exception {
		
		Group root = new Group();
		BetsGame round = new BetsGame(250);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 800,300);
		scene.setFill(Color.BLACK);
		
		//Associate scene to stage and show
		stage.setTitle("Money bets");
		stage.setScene(scene);
		
		stage.show();
		
		HBox buttons = new HBox();
		Button even = new Button("Even");
		Button odd = new Button("Odd");
		buttons.getChildren().addAll(even,odd);
		//even.setOnAction();
		
		HBox textFields = new HBox();	
		TextField bet = new TextField("bet: ");
		TextField moneyLeftText = new TextField("Money left: "+round.getMoney() );
		message = new TextField("Even or Odd?");
		message.setPrefWidth(300);

		//message.setText();
		textFields.getChildren().addAll(message,bet,moneyLeftText);

		VBox all = new VBox(buttons,textFields);
		root.getChildren().add(all);
		
		int userBet = Integer.parseInt(bet.getText());
		BetsChoice onAction1 = new BetsChoice(message, userBet, moneyLeftText, "Even", round);
		BetsChoice onAction2= new BetsChoice(message, userBet, moneyLeftText, "Odd", round);
		
		even.setOnAction(onAction1);
		odd.setOnAction(onAction2);
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
