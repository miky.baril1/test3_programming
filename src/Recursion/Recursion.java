package Recursion;

public class Recursion {

	public static void main (String[]args) {
		
		String[] array = null;
		recursiveCount(array, 0);
	}
	public static int recursiveCount(String[]array, int n) {
		int counter = 0;
		
		for(int i =n;i<array.length;i=i+2) {
			
			if(i%2!=0) {
				i++;
			}
			String text= array[i];
			
			for(int j=0;j<text.length();j++) {
				
				if(text.charAt(i)=='q') {
					counter++;
				}
			}
		}
		
		return counter;
		
	}
	
}
