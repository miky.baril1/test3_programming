package Recursion;

public class TeacherRecur {
public static void main (String[]args) {
	
	System.out.print(foo(5));

}
public static int foo(int n) {

    if (n <= 0) {

           return Math.abs(n);

    }

   

    int x = foo(n-2);

    int y = foo(n-1);

    int z = foo(n-2);

    return x + y + z;
}
}
