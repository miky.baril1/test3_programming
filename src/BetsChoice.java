import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class BetsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField moneyLeftText;
	private int userBet;
	private BetsGame round;
	private String pChoice;
	public BetsChoice(TextField message,  int userBet, TextField moneyLeftText,String pChoice, BetsGame round) {
		this.message=message;
		this.moneyLeftText=moneyLeftText;
		this.userBet=userBet;
		this.pChoice=pChoice;
		this.round=round;
	}
	
	
	public void handle(ActionEvent arg0) {
		String result = round.placeBet(pChoice,userBet);
		message.setText(result);
		moneyLeftText.setText("Money left : "+ round.getMoney());
		
	}
}
